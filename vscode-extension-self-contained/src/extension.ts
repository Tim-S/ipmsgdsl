'use strict';

import * as path from 'path';
import * as os from 'os';

import {Trace} from 'vscode-jsonrpc';
import { commands, window, workspace, ExtensionContext, Uri } from 'vscode';
import { LanguageClient, LanguageClientOptions, ServerOptions } from 'vscode-languageclient';

import * as child_process from 'child_process';
import * as vscode from 'vscode';

export function activate(context: ExtensionContext) {
    // The server is a locally installed in src/ipmsgdsl
    let launcher = os.platform() === 'win32' ? 'de.schneidertim.ipmsg.dsl.ide.bat' : 'de.schneidertim.ipmsg.dsl.ide';
    let script = context.asAbsolutePath(path.join('src', 'ipmsgdsl', 'bin', launcher));

    let serverOptions: ServerOptions = {
        run : { command: script },
        debug: { command: script, args: [], options: { env: createDebugEnv() } }
    };
    
    let clientOptions: LanguageClientOptions = {
        documentSelector: ['ipmsgdsl'],
        synchronize: {
            fileEvents: workspace.createFileSystemWatcher('**/*.*')
        }
    };
    
    // Create the language client and start the client.
    let lc = new LanguageClient('IpMsgDsl Language Server', serverOptions, clientOptions);
    
    var disposable2 =commands.registerCommand("ipmsgdsl.a.proxy", async () => {
        let activeEditor = window.activeTextEditor;
        if (!activeEditor || !activeEditor.document || activeEditor.document.languageId !== 'ipmsgdsl') {
            return;
        }

        if (activeEditor.document.uri instanceof Uri) {
            commands.executeCommand("ipmsgdsl.a", activeEditor.document.uri.toString());
        }
    })
    context.subscriptions.push(disposable2);

    let generator_launcher = os.platform() === 'win32' ? 'de.schneidertim.ipmsg.dsl.generator.bat' : 'de.schneidertim.ipmsg.dsl.generator';
    let generator_script = context.asAbsolutePath(path.join('src', 'ipmsgdsl', 'bin', generator_launcher));
    var disposable3 = commands.registerCommand('ipmsgdsl.codegen', async () => {
        let activeEditor = window.activeTextEditor;
        if (!activeEditor || !activeEditor.document || activeEditor.document.languageId !== 'ipmsgdsl') {
            return;
        }

        if (activeEditor.document.uri instanceof Uri) {

            //Create output channel
            const outputChannel = vscode.window.createOutputChannel("IpMsgDsl Code Generator");
            outputChannel.show();

            const fileName = activeEditor.document.fileName
            outputChannel.appendLine(`Running ipmsgdsl.codegen on ${fileName}`)
            outputChannel.appendLine(activeEditor.document.fileName)

            const workSpaceFolder = vscode.workspace.getWorkspaceFolder(activeEditor.document.uri)
            const cwd = vscode.workspace.asRelativePath(workSpaceFolder.uri)
            const proc = child_process.execFileSync(generator_script, [fileName], {cwd: cwd});
            outputChannel.appendLine(String(proc));
        }
    })
    context.subscriptions.push(disposable3);
    
    // enable tracing (.Off, .Messages, Verbose)
    lc.trace = Trace.Verbose;
    let disposable = lc.start();
    
    // Push the disposable to the context's subscriptions so that the 
    // client can be deactivated on extension deactivation
    context.subscriptions.push(disposable);
}

function createDebugEnv() {
    return Object.assign({
        JAVA_OPTS:"-Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=8000,suspend=n,quiet=y"
    }, process.env)
}