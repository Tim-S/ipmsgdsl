## (Re-)Building Docker Image for Offfline Builds

To create an image that can be used to rebuild the extension offline later on, run
```
docker build --no-cache -t <your-project-image-name-vsce> -f docker/vsce.docker .
```

_Note_ : We include a 

## Building VSIX package for VSCode
```
docker run -it --rm -v ${PWD}:/workspace -v /workspace/node_modules <your-project-image-name-vsce>
```
