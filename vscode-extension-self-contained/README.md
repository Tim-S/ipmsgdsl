# Xtext VS Code example

## Build VS Code Extensions

Run the following command from the project's root
```
.\gradlew vscodeExtension
```

an a successfully build you will find a *.vsix in _.vscode-extension-self-code/build/vscode_
